package neuralnetwork.ui;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.ObjectBinding;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.util.Pair;
import neuralnetwork.*;
import scala.Tuple2;
import scala.Tuple3;
import scala.collection.JavaConverters;
import scala.collection.immutable.List;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

public class FXController {
    public static final int XMIN = -10;
    public static final int XMAX = 10;
    public static final int XRES = 100;
    public static final int YMIN = -10;
    public static final int YMAX = 10;
    public static final int YRES = 500;


    @FXML
    private VBox rightRootVBox;
    @FXML
    private ChoiceBox<String> dataGenChoice;
    @FXML
    private TextField numOfTrain;
    @FXML
    private TextField numOfTest;
    @FXML
    private VBox inputBlock;
    @FXML
    private HBox outputDimHBox;
    @FXML VBox hiddenLayerBlock;
    @FXML
    private Slider inputSlider;
    @FXML
    private Slider outputSlider;
    @FXML
    private Slider hiddenLayerSlider;
    @FXML
    private Button startButton;
    @FXML
    private VBox generatorVBox;
    @FXML
    private Accordion dataGenAddSettings;
    @FXML
    private ChoiceBox<String> outputActFunChoice;
    @FXML HBox outputActFunHBox;
    @FXML
    private TextField outputActFunA;
    @FXML
    private VBox hiddenLayersVBox;
    @FXML
    private Label isDone;
    @FXML
    private VBox resultBlock;
    @FXML
    private Label mseLabel;
    @FXML
    private Label timeLabel;
    @FXML
    private VBox controlBlock;
    @FXML
    private Group coordinateSystem;
    @FXML
    private ChoiceBox<String> etaChoice;
    @FXML
    private TextField etaNull;
    @FXML
    private TextField theta;

    private final ObservableList<String> actFunctions = FXCollections.observableArrayList("Logistic function", "Hyberbolic tangent");
    private final ObservableList<String> etaFunctions = FXCollections.observableArrayList("Constant", "Constant over n", "Search then converge");

    private final IntegerProperty numOfInputNeurons = new SimpleIntegerProperty(0);
    private final IntegerProperty numOfOutputNeurons = new SimpleIntegerProperty(0);
    private ObservableList<ObjectBinding<Tuple3<Integer,Integer,String>>> hiddenLayerParams;
    private final DoubleProperty sampleSourcesOpacity = new SimpleDoubleProperty(1.0);
    private DataGenerator dataGenerator;

    private VBox moonSettings;

    private final ProgressBar progressBar = new ProgressBar();
    private boolean startStopToggle = false;

    private Path path;
    private final ObservableList<DoubleProperty> functionValues = FXCollections.observableArrayList();

    @FXML public void initialize() {
        progressBar.setMinHeight(20);
        progressBar.setMinWidth(200);

        ObservableList<String> ol = FXCollections.observableArrayList("Half-moons");
        dataGenChoice.setItems(ol);

        inputSlider.valueProperty().bindBidirectional(numOfInputNeurons);
        outputSlider.valueProperty().bindBidirectional(numOfOutputNeurons);

        dataGenChoice.getSelectionModel().selectedIndexProperty().addListener((ov, oldVal, newVal) -> {
            if(newVal.equals(0)) {
                inputBlock.setDisable(true);
                outputDimHBox.setDisable(true);
                startButton.setDisable(false);

                numOfInputNeurons.setValue(2);
                numOfOutputNeurons.setValue(1);

                FXSettingsController moon1Controller = new FXSettingsController();
                FXSettingsController moon2Controller = new FXSettingsController();

                try {
                    //Load additional settings for specified data generator type
                    FXMLLoader fxmlLoader = new FXMLLoader();
                    fxmlLoader.setLocation(FXController.class.getResource("/moonSettings.fxml"));
                    fxmlLoader.setController(moon1Controller);
                    VBox moon1 = fxmlLoader.load();

                    fxmlLoader = new FXMLLoader();
                    fxmlLoader.setLocation(FXController.class.getResource("/moonSettings.fxml"));
                    fxmlLoader.setController(moon1Controller);
                    fxmlLoader.setController(moon2Controller);
                    VBox moon2 = fxmlLoader.load();

                    moonSettings = new VBox(moon1, moon2);

                    dataGenAddSettings.getPanes().add(new TitledPane("Hold 1", moon1));
                    dataGenAddSettings.getPanes().add(new TitledPane("Hold 2", moon2));

                    //Display the area from which random points will be selected as samples
                    HalfMoon hm1 = new HalfMoon(moon1Controller.centX, moon1Controller.centY, moon1Controller.rad, moon1Controller.width, moon1Controller.angle, Color.RED);
                    HalfMoon hm2 = new HalfMoon(moon2Controller.centX, moon2Controller.centY, moon2Controller.rad, moon2Controller.width, moon2Controller.angle, Color.BLUE);

                    hm1.opacityProperty().bind(sampleSourcesOpacity);
                    hm2.opacityProperty().bind(sampleSourcesOpacity);

                    moon1Controller.centX.set(-1.0);
                    moon2Controller.centX.set(1.0);
                    moon2Controller.centY.set(-1.0);
                    moon2Controller.angle.set(3.15);

                    //X Y axes
                    Line xCoord = new Line(-60.0,0.0,60.0,0.0);
                    Line yCoord = new Line(0.0, -60.0, 0.0, 60.0);
                    xCoord.setStrokeWidth(.06);
                    yCoord.setStrokeWidth(.06);

                    Group group = new Group(xCoord, yCoord, hm1, hm2);
                    group.setScaleX(45);
                    group.setScaleY(45);
                    coordinateSystem.getChildren().add(group);

                    dataGenerator = new DataGenerator(hm1::isInside, hm2::isInside, XMIN, XMAX, YMIN, YMAX);
                }
                catch (IOException e) {
                    System.out.println("Couldn't load FXML file: " + e);
                }
            }
            else {
                inputBlock.setDisable(false);
                outputDimHBox.setDisable(false);

                coordinateSystem.getChildren().clear();
                generatorVBox.getChildren().remove(generatorVBox.getChildren().size()-1); //remove last children
            }
        });
        //Set half moons as default
        dataGenChoice.setValue(ol.get(0));

        etaChoice.setItems(etaFunctions);
        etaChoice.getSelectionModel().selectedIndexProperty().addListener((ov, oldVal, newVal) -> {
            if(newVal.equals(2))
                theta.setVisible(true);
            else
                theta.setVisible(false);
        });
        etaChoice.setValue(etaFunctions.get(2));

        outputActFunChoice.setItems(actFunctions);
        outputActFunChoice.getSelectionModel().selectedIndexProperty().addListener((ov, oldVal, newVal) -> {
            //Some activation functions have additional params, which can be read from a text field visible during such selections
            if(newVal.equals(0)) {
                outputActFunA.setVisible(true);
            }
            else {
                outputActFunA.setVisible(false);
            }
        });
        //Set hyperbolic function as default
        outputActFunChoice.setValue(actFunctions.get(1));

        //Parameters of dynamically loaded hidden layer settings
        //It's a scala 3 elems Tuple, first one is function index, second one is number of neurons in layer, third is the string repr. of the optional parameter
        hiddenLayerParams = FXCollections.observableArrayList();
        hiddenLayerSlider.valueProperty().addListener((ov, oldVal, newVal) -> {
            ObservableList<Node> children = hiddenLayersVBox.getChildren();
            children.clear();

            for(int i = 0; i < (int) newVal.doubleValue(); i++) {
                Label label = new Label("Layer " + (i+1));
                label.getStyleClass().add("title");

                Label actFun = new Label("Activation function:");
                ChoiceBox<String> choiceBox = new ChoiceBox<>();
                choiceBox.setItems(actFunctions);
                TextField textField = new TextField();
                //if this layer was previously set, restore those settings
                if(i < hiddenLayerParams.size())
                    textField.setText(hiddenLayerParams.get(i).get()._3());
                textField.setMaxWidth(60);
                textField.setVisible(false);
                HBox hbox1 = new HBox(actFun, choiceBox, textField);

                choiceBox.getSelectionModel().selectedIndexProperty().addListener((obsv, oldValue, newValue) -> {
                    if(newValue.equals(0)) {
                        textField.setVisible(true);
                    }
                    else {
                        textField.setVisible(false);
                    }
                });
                if(i >= hiddenLayerParams.size())
                    choiceBox.setValue(actFunctions.get(1));
                else //if this layer was previously set, restore those settings
                    choiceBox.setValue(actFunctions.get(hiddenLayerParams.get(i).get()._1()));


                Label numNeur = new Label("Num of neurons:");
                Slider slider = new Slider(1,100,20);
                slider.setMajorTickUnit(10);
                slider.setMinorTickCount(5);
                slider.setShowTickLabels(true);
                slider.setShowTickMarks(true);
                slider.setBlockIncrement(1);
                if(i < hiddenLayerParams.size())
                    slider.setValue(hiddenLayerParams.get(i).get()._2());
                HBox hbox2 = new HBox(numNeur, slider);


                //dependencies for the binding after
                ReadOnlyIntegerProperty index = choiceBox.getSelectionModel().selectedIndexProperty();
                DoubleProperty sliderVal = slider.valueProperty();

                //It's a scala 3 elems Tuple, first one is function index, second one is number of neurons in layer, third is the string repr. of the optional parameter
                ObjectBinding<Tuple3<Integer,Integer,String>> binding = Bindings.createObjectBinding(() -> new Tuple3<>(index.get(),(int) sliderVal.get(),textField.textProperty().get()),
                        index, sliderVal, textField.textProperty()); //these last three are the dependencies, on their change, the value will be recalculated
                //Add to arraylist if no previous tuple of this index was added, overwrite otherwise (necessary because the binding's dependencies have changed as well
                if(hiddenLayerParams.size() <= i)
                    hiddenLayerParams.add(binding);
                else
                    hiddenLayerParams.set(i, binding);

                VBox vbox = new VBox(label, hbox1, hbox2);
                children.add(vbox);
            }
        });
        hiddenLayerSlider.setValue(1);

        path = new Path();
        for(int i = 0; i < XRES; i++) {
            DoubleProperty sdp = new SimpleDoubleProperty();
            double x = XMIN+i*(XMAX-XMIN)/(double) XRES;
            if(i == 0) {
                MoveTo mt = new MoveTo();
                mt.setX(x);
                mt.yProperty().bind(sdp);
                path.getElements().add(mt);
            }
            else {
                LineTo lt = new LineTo();
                lt.setX(x);
                lt.yProperty().bind(sdp);
                path.getElements().add(lt);
            }
            functionValues.add(sdp);
        }
        path.setStrokeWidth(.07);
        path.setVisible(false);
        ((Group) coordinateSystem.getChildren().get(0)).getChildren().add(path);
    }
    private Trainable.EtaFunction choiceToEta(int i, TextField tf, TextField tf2) {
        switch (i) {
            case 0 : return new Trainable.ConstantEta(checkIfParsable(tf));
            case 1 : return new Trainable.ConstantOverNEta(checkIfParsable(tf));
            default: return new Trainable.SearchThenConverge(checkIfParsable(tf), checkIfParsable(tf2));
        }
    }
    private Differentiablity choiceToFunc(int i, TextField tf) {
        switch(i) {
            case 0 : return new LogisticFunction(checkIfParsable(tf));
            default: return HyberbolicTangent$.MODULE$;
        }
    }
    private Differentiablity choiceToFunc(int i, String tf) {
        switch(i) {
            case 0 : return new LogisticFunction(Double.parseDouble(tf));
            default: return HyberbolicTangent$.MODULE$;
        }
    }
    //With the thrown exception, the training wil not start, and this function will show which text field contains the non-parsable value by turning it red for a second
    private double checkIfParsable(TextField tf) throws NumberFormatException {
        try {
            double ret;
            ret = Double.parseDouble(tf.getText());
            return ret;
        }
        catch (NumberFormatException e) {
            tf.setStyle("-fx-background-color: red;");
            new Thread(() -> {
                try { Thread.sleep(1000); } catch (Exception ignored) {}
                Platform.runLater(() -> tf.setStyle(""));
            }).start();

            throw e;
        }
    }
    private final ObservableList<Circle> samplePoints = FXCollections.observableArrayList();
    private Thread thread = new Thread();


    @FXML public void startTraining() { //Called from the UI's start training button
        if(startStopToggle) { //STOP
            thread.interrupt();
            controlBlock.getChildren().remove(progressBar);
            isDone.setVisible(false);
            resultBlock.setVisible(false);
            startButton.setText("Start training");
            startStopToggle = !startStopToggle;
        }
        else { //START
            try {
                path.setVisible(true);
                ((Group) coordinateSystem.getChildren().get(0)).getChildren().removeAll(samplePoints);
                samplePoints.clear();
                isDone.setVisible(false);
                resultBlock.setVisible(false);

                int numOfTrainSamples = (int) checkIfParsable(numOfTrain);
                int numOfTestSamples = (int) checkIfParsable(numOfTest);
                startButton.setText("Stop training");
                controlBlock.getChildren().add(progressBar);
                sampleSourcesOpacity.set(.3);

                ObjectProperty<MultilayerPerceptron> nn = new SimpleObjectProperty<>();

                DoubleProperty timeTook = new SimpleDoubleProperty();
                Task<Object> task = new Task<Object>() {
                    @SuppressWarnings("unchecked")
                    @Override
                    protected Object call() {
                        List<Tuple2<double[], double[]>> trainingSample = dataGenerator.generate(numOfTrainSamples);
                        List<Tuple2<double[], double[]>> testSample = dataGenerator.generate(numOfTestSamples);

                        //Display training samples
                        for (int i = 0; i < trainingSample.length(); i++) {
                            Tuple2<double[], double[]> sample = trainingSample.apply(i);
                            Circle circle = new Circle(sample._1[0], sample._1[1], .06);
                            if (sample._2[0] == 1.0)
                                circle.setFill(Color.RED);
                            else
                                circle.setFill(Color.BLUE);
                            samplePoints.add(circle);

                            //Add points on application thread
                            Platform.runLater(() -> ((Group) coordinateSystem.getChildren().get(0)).getChildren().add(circle));
                        }

                        //Create an array of pairs
                        Pair<Object, Differentiablity>[] hiddenLayers = new Pair[(int) hiddenLayerSlider.getValue()];
                        for (int i = 0; i < hiddenLayers.length; i++) {
                            //It's a scala 3 elems Tuple, first one is function index, second one is number of neurons in layer, third is the string repr. of the optional parameter
                            hiddenLayers[i] = new Pair<>(hiddenLayerParams.get(i).get()._2(), choiceToFunc(hiddenLayerParams.get(i).get()._1(),
                                    hiddenLayerParams.get(i).get()._3()));
                        }

                        //Create neural network with specified parameters. Note that nn is an ObjectProperty
                        nn.setValue(new MultilayerPerceptron((int) inputSlider.getValue(), (int) outputSlider.getValue(), choiceToFunc(outputActFunChoice.getSelectionModel().getSelectedIndex(), outputActFunA),
                                hiddenLayers, trainingSample, testSample, choiceToEta(etaChoice.getSelectionModel().getSelectedIndex(), etaNull, theta), functionValues));


                        //The neural network will change the n IntegerProperty according to it's current training sample
                        IntegerProperty n = new SimpleIntegerProperty(0);
                        n.addListener(e -> updateProgress(n.get(), trainingSample.length()));
                        long time1 = System.nanoTime();
                        nn.get().trainNetwork(1, 1, n);
                        long time2 = System.nanoTime();
                        timeTook.set((time2-time1)/1e9);

                        return null;
                    }
                };

                progressBar.progressProperty().bind(task.progressProperty());

                //After the training succeeded remove progressBar
                task.setOnSucceeded(e -> {
                    controlBlock.getChildren().remove(progressBar);
                    isDone.setVisible(true);
                    double MSE = nn.get().testNetwork();
                    resultBlock.setVisible(true);
                    mseLabel.setText(Double.toString(MSE));
                    timeLabel.setText(Double.toString(timeTook.get()) + "s");

                    startStopToggle = false;
                    controlBlock.getChildren().remove(progressBar);
                    startButton.setText("Start training");
                });
                task.exceptionProperty().addListener((ob, oldVal, newVal) -> {
                    if(newVal instanceof Exception) {
                        newVal.printStackTrace();
                    }
                    if(newVal instanceof Error) {
                        newVal.printStackTrace();
                    }
                });
                thread = new Thread(task);
                thread.start();

                startStopToggle = !startStopToggle;
            } catch (NumberFormatException e) {
                System.out.println("Couldn't start training");
            }
        }
    }
}
