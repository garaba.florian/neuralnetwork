package neuralnetwork.ui

import javafx.beans.binding.Bindings
import javafx.beans.property.DoubleProperty
import javafx.scene.paint.Color
import javafx.scene.shape._

class HalfMoon(var centX : DoubleProperty, var centY : DoubleProperty, var rad : DoubleProperty, var width : DoubleProperty, var angle : DoubleProperty, var color : Color) extends Path {
  val moveTo = new MoveTo()
  //val cos = Math.cos(angle.get())
  //val sin = Math.sin(angle.get())
  private val cos = Bindings.createDoubleBinding(() => {
    Math.cos(angle.get())
  }, angle)
  private val sin = Bindings.createDoubleBinding(() => {
    Math.sin(angle.get())
  }, angle)
  moveTo.xProperty.bind(centX.add(rad.multiply(cos)))
  moveTo.yProperty.bind(centY.add(rad.multiply(sin)))

  val arcTo = new ArcTo()
  arcTo.xProperty.bind(centX.subtract(rad.multiply(cos)))
  arcTo.yProperty.bind(centY.subtract(rad.multiply(sin)))
  arcTo.XAxisRotationProperty().bind(angle)
  arcTo.radiusXProperty().bind(rad)
  arcTo.radiusYProperty().bind(rad)

  val lineTo = new LineTo()
  lineTo.xProperty.bind(centX.subtract(rad.subtract(width).multiply(cos)))
  lineTo.yProperty.bind(centY.subtract(rad.subtract(width).multiply(sin)))

  val arcTo2 = new ArcTo()
  arcTo2.xProperty.bind(centX.add(rad.subtract(width).multiply(cos)))
  arcTo2.yProperty.bind(centY.add(rad.subtract(width).multiply(sin)))
  arcTo2.XAxisRotationProperty().bind(angle)
  arcTo2.radiusXProperty().bind(rad.subtract(width))
  arcTo2.radiusYProperty().bind(rad.subtract(width))
  arcTo2.setSweepFlag(true)

  val lineTo2 = new LineTo()
  lineTo2.xProperty.bind(centX.add(rad.multiply(cos)))
  lineTo2.yProperty.bind(centY.add(rad.multiply(sin)))

  getElements.add(moveTo)
  getElements.add(arcTo)
  getElements.add(lineTo)
  getElements.add(arcTo2)
  getElements.add(lineTo2)
  setFill(color)
  setStrokeWidth(.1)

  def isInside(x : Double, y : Double): Boolean = {
    //val piFactor = angle.get() % Math.PI
    val piFactor = angle.get()/Math.PI// - Math.floor(angle.get()/(2*Math.PI))
    lazy val circleStripe = Math.pow(x - centX.get(), 2) + Math.pow(y - centY.get(), 2) <= Math.pow(rad.get(), 2) &&
        Math.pow(x - centX.get(), 2) + Math.pow(y - centY.get(), 2) >= Math.pow(rad.get() - width.get(), 2)
    if(piFactor <= .5 || piFactor >= 1.5) {
      if (Math.tan(angle.get()) * x + centY.get() - Math.tan(angle.get()) * centX.get() > y) {
        circleStripe
      }
      else false
    }
    else {
      if (Math.tan(angle.get()) * x + centY.get() - Math.tan(angle.get()) * centX.get() < y) {
        circleStripe
      }
      else false
    }
  }
}
