package neuralnetwork.ui;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Slider;

class FXSettingsController {
    @FXML
    private Slider centXSlider;
    @FXML
    private Slider centYSlider;
    @FXML
    private Slider radSlider;
    @FXML
    private Slider widthSlider;
    @FXML
    private Slider angleSlider;

    final DoubleProperty centX = new SimpleDoubleProperty(0.0);
    final DoubleProperty centY = new SimpleDoubleProperty(0.0);
    final DoubleProperty rad = new SimpleDoubleProperty(3.0);
    final DoubleProperty width = new SimpleDoubleProperty(1.2);
    final DoubleProperty angle = new SimpleDoubleProperty(0.0);

    @FXML public void initialize() {
        centXSlider.valueProperty().bindBidirectional(centX);
        centYSlider.valueProperty().bindBidirectional(centY);
        radSlider.valueProperty().bindBidirectional(rad);
        widthSlider.valueProperty().bindBidirectional(width);
        angleSlider.valueProperty().bindBidirectional(angle);
    }
}
