package neuralnetwork

import scala.concurrent.duration._

abstract class Edge {
  val inputs : List[Edge]
  def delay : Duration = 0.milli
  def valueAt(n: Duration): Double = {
    if(n-delay>=0.milli) (for(i <- inputs) yield i.valueAt(n-delay)).sum
    else 0.0
  }
  def value : Double = inputs.map(_.value).sum
}

trait FunctionEdge extends Edge {
  val activationFunction : ActivationFunction
  def localFieldAt(n : Duration) : Double = super.valueAt(n)
  abstract override def valueAt(n : Duration) = activationFunction(super.valueAt(n))
  def localField: Double = super.value
  abstract override def value = activationFunction(super.value)
}
trait CachedEdge extends Edge {
  private var _value : Option[Double] = None
  def deleteCache() : Unit = _value = None
  abstract override def value: Double = { if(_value.isEmpty) { _value = Option(super.value) }; _value.get }
}
class InputEdge(var input : Double) extends Edge {
  override val inputs: List[Edge] = Nil
  override def valueAt(n: Duration): Double = if(n-delay>=0.milli) input else 0.0
  override def value: Double = input
}

sealed abstract class ActivationFunction {
  def apply(x : Double) : Double
}
trait Differentiablity extends ActivationFunction {
  def derivative(x : Double) : Double
}
case class LinearFunction(var a : Double = 1.0) extends ActivationFunction with Differentiablity {
  def apply(x : Double) : Double = a*x
  def derivative(x : Double) : Double = a
}
object ThresholdFunction extends ActivationFunction {
  def apply(x : Double) : Double  = if(x >= 0) 1.0 else 0.0
}
object ThresholdFunctionMinToPos extends ActivationFunction {
  def apply(x : Double) : Double = if(x > 0) 1.0 else -1.0 //TODO: Actually not defined for 0, maybe change to partial function
}
case class LogisticFunction(a : Double = 1.0) extends ActivationFunction with Differentiablity {
  def apply(x : Double) : Double = 1/(1+Math.exp(-a*x))
  def derivative(x : Double) : Double = a*Math.exp(-a*x)/Math.pow(1+Math.exp(-a*x),2)
}
object HyberbolicTangent extends ActivationFunction with Differentiablity {
  def apply(x : Double) : Double = (1-Math.exp(-2*x))/(1+Math.exp(-2*x))
  def derivative(x: Double): Double = 1-Math.pow(apply(x),2)
}
