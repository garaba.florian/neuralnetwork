package neuralnetwork

import scala.util.Random

class DataGenerator(val group1 : (java.lang.Double,java.lang.Double) => Boolean, val group2 : (java.lang.Double,java.lang.Double) => Boolean, val xMin : Double, val xMax : Double, val yMin : Double, val yMax : Double) {
  def generate(n : Int) : List[(Array[Double], Array[Double])] = {
    println(s"Generating $n data")

    var res = List[(Array[Double], Array[Double])]()
    var x = 0.0
    var y = 0.0
    do {
      x = Random.nextDouble()*(xMax-xMin)-(xMax-xMin)/2
      y = Random.nextDouble()*(yMax-yMin)-(xMax-xMin)/2
      if(group1(x,y)) {
        res = (Array(x, y), Array(1.0)) :: res
      }
      else if(group2(x,y)) {
        res = (Array(x, y), Array(-1.0)) :: res
      }
    } while(res.length < n)
    res
  }
}
