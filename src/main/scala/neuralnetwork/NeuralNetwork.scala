package neuralnetwork

import javafx.application.Platform
import javafx.beans.property.{DoubleProperty, IntegerProperty}
import javafx.collections.{ObservableArray, ObservableList}
import neuralnetwork.ui.FXController

import scala.util.Random
import scala.util.control.Breaks._

abstract class NeuralNetwork {
  class WeightEdge(inputEdge : Edge, initialWeight : Double) extends Edge with FunctionEdge {
    private var _weight : Double = initialWeight
    override val inputs: List[Edge] = List(inputEdge)
    override val activationFunction: LinearFunction = LinearFunction(initialWeight)
    def weight : Double = _weight
    def weight_=(x : Double) : Unit = { _weight = x; activationFunction.a = x }
    def input : Edge = inputs.head
  }
  class Neuron(input : List[Edge], initialWeights : Seq[Double], override val activationFunction: ActivationFunction) extends Edge with FunctionEdge {
    val inputs : List[WeightEdge] = {
      for((edge, index) <- (new InputEdge(1.0) :: input).zipWithIndex) yield new WeightEdge(edge, initialWeights(index))
    }
    def inputNeurons : List[Neuron] = input.filter(_.isInstanceOf[Neuron]).asInstanceOf[List[Neuron]]
    def isOutputNeuron : Boolean = outputLayer.contains(this)
    def outputNeurons : List[Neuron] = neurons.filter(_.inputNeurons.contains(this))
  }
  class BackpropNeuron(input : List[Edge], initialWeights : Seq[Double], override val activationFunction: Differentiablity) extends Neuron(input, initialWeights, activationFunction) {
    private var _gradient : Option[Double] = None
    def gradient(result : Array[Double]) : Double = { if(_gradient.isEmpty) { _gradient = Option(gradient_(result)) }; _gradient.get }
    def deleteCache(): Unit = _gradient = None

    private def gradient_(result : Array[Double]) : Double = {
      if(isOutputNeuron) {
        val indexOfNeuron = outputLayer.zipWithIndex.find(a => a._1 == this).get._2
        activationFunction.derivative(localField) * (result(indexOfNeuron) - value)
      }
      else {
        activationFunction.derivative(localField)*
          outputNeurons.asInstanceOf[List[BackpropNeuron]].map(n => n.gradient(result)*n.inputs.find(_.input==this).get.weight).sum
      }
    }
  }
  def streamOfRandom : Stream[Double] = (Random.nextDouble()*2-1.0) #:: streamOfRandom


  val inputDimensionality : Int
  val outputDimensionality : Int

  def createNetwork() : (List[InputEdge], List[Edge], List[Neuron])
  val (inputLayer, outputLayer, neurons)  = createNetwork()
  require(inputDimensionality == inputLayer.length && outputDimensionality == outputLayer.length)

  private[this] def loadData(data : Seq[Double]) : Unit = {
    require(data.length == inputDimensionality)
    for(n <- neurons if n.isInstanceOf[BackpropNeuron])
      n.asInstanceOf[BackpropNeuron].deleteCache()
    for((inputEdge, data) <- inputLayer.zip(data)) {
      inputEdge.input = data
    }
  }
  def output(data : Seq[Double]) : List[Double] = {
    loadData(data)
    for(o <- outputLayer) yield o.value
  }

  def drawNetworkStructure() : Unit = ???
}
object Trainable {
  abstract class EtaFunction {
    def apply(n : Int) : Double
  }
  case class ConstantEta(etaNull : Double) extends EtaFunction {
    override def apply(n: Int): Double = etaNull
  }
  case class ConstantOverNEta(c : Double) extends EtaFunction {
    override def apply(n: Int): Double = c/n.toDouble
  }
  case class SearchThenConverge(etaNull : Double, theta : Double) extends EtaFunction {
    override def apply(n: Int): Double = etaNull/(1+(n/theta))
  }
}
trait Trainable extends NeuralNetwork {
  val trainingSample : Seq[(Array[Double], Array[Double])]
  val testSample : Seq[(Array[Double], Array[Double])]
  require(inputDimensionality == trainingSample.head._1.length && outputDimensionality == trainingSample.head._2.length)
  val functionPoints : ObservableList[DoubleProperty]

  val eta : Trainable.EtaFunction
  def trainNetwork(epochs : Int, N : Int, n : IntegerProperty) : Unit = {
    breakable {
      for (epoch <- 1 to epochs) {
        for ((epochTrainingSample, epochIndex) <- trainingSample.sliding(N).zipWithIndex; ((data, result), trainIndex) <- epochTrainingSample.zipWithIndex) {
          if (Thread.currentThread().isInterrupted) {
            println("Training interrupted")
            break
          }
          n.set(epochIndex + 1)
          trainBody(data, result, n.get())

          if(n.get() % 50 == 0) {
            var ys : List[Double] = List()
            var i = 0
            while (i < functionPoints.size()) {
              var outOfOld = 0
              var change = false
              var y: Double = FXController.YMIN
              do {
                y += (FXController.YMAX - FXController.YMIN) / FXController.YRES.toDouble
                val out = if(output(List(FXController.XMIN + i * (FXController.XMAX - FXController.XMIN) / FXController.XRES.toDouble, y)).head > 0) 1 else -1
                change = if(out == -outOfOld) true else false
                outOfOld = out
              } while (y <= FXController.YMAX && !change)
              ys = y :: ys
              i += 1
            }
            Platform.runLater(() => {
              for((y,i) <- ys.reverse.zipWithIndex) //Scala lists are prepended, so we have to reverse it
                functionPoints.get(i).set(y)
            })
          }
        }
        println(s"MSE: " + testNetwork())
      }
    }
    if(Thread.currentThread().isInterrupted)
      println("Network's training has been interrupted.")
    else
      println("Network has been trained. Resulting parameters: ")
    println((for (neur <- neurons; input <- neur.inputs; if input.isInstanceOf[WeightEdge]) yield input.weight).mkString(" "))
  }
  def trainBody(data : Array[Double], result : Array[Double], n : Int)
  def testNetwork() : Double = {
    val seqOfCosts = for((data, result) <- testSample) yield {
      result.zip(output(data)).map(a => Math.pow(a._1 - a._2, 2)).sum
    }
    val MSE = seqOfCosts.sum/testSample.size
    //println(s"MSE: $MSE over ${testSample.size} data")
    MSE
  }
}

class MultilayerPerceptron(override val inputDimensionality: Int, override val outputDimensionality: Int, val outputFunc : Differentiablity, val hiddenLayers : Array[javafx.util.Pair[Int, Differentiablity]],
                           override val trainingSample: Seq[(Array[Double], Array[Double])],
                           override val testSample: Seq[(Array[Double], Array[Double])], val eta : Trainable.EtaFunction,
                           override val functionPoints: ObservableList[DoubleProperty]) extends NeuralNetwork with Trainable {
  override def createNetwork(): (List[InputEdge], List[Edge], List[Neuron]) = {
    val inputLayer = for(_ <- (1 to inputDimensionality).toList) yield new InputEdge(1.0)
    var neurons = List[BackpropNeuron]()
    for((l, index) <- hiddenLayers.zipWithIndex) {
      for(i <- 0 until l.getKey) {
        neurons = new BackpropNeuron(if (index == 0) inputLayer else neurons.slice(i, hiddenLayers(index-1).getKey), streamOfRandom, hiddenLayers(index).getValue) :: neurons
      }
    }
    val outputLayer = for(i <- (1 to outputDimensionality).toList) yield new BackpropNeuron(if(neurons.nonEmpty) neurons.take(hiddenLayers.last.getKey) else inputLayer, streamOfRandom, outputFunc)
    (inputLayer, outputLayer, outputLayer ::: neurons)
  }

  //val eta = new Trainable.ConstantEta(.001)
  //val eta = Trainable.SearchThenConverge(.1, 100)
  override def trainBody(data: Array[Double], result: Array[Double], n: Int): Unit = {
    output(data)
    val deltaWs = new Array[Array[Double]](neurons.length)
    for((neur,i) <- neurons.zipWithIndex) {
      deltaWs(i) = new Array[Double](neur.inputs.length)
      for((weightEdge,j) <- neur.inputs.zipWithIndex) {
        //weightEdge.weight = weightEdge.weight + eta(n)*neur.asInstanceOf[BackpropNeuron].gradient(result)*weightEdge.input.value
        deltaWs(i)(j) = eta(n)*neur.asInstanceOf[BackpropNeuron].gradient(result)*weightEdge.input.value
      }
    }
    for((neur,i) <- neurons.zipWithIndex; (weightEdge,j) <- neur.inputs.zipWithIndex) {
      weightEdge.weight = weightEdge.weight + deltaWs(i)(j)
    }
  }
}


/*class Perceptron(override val inputDimensionality: Int, override val outputDimensionality: Int,
                 override val trainingSample: Seq[(Array[Double], Array[Double])],
                 override val testSample: Seq[(Array[Double], Array[Double])] ) extends NeuralNetwork with Trainable {
  override def createNetwork(): (List[InputEdge], List[Edge], List[Neuron]) = {
    val inputs = for(_ <- (1 to inputDimensionality).toList) yield new InputEdge(0.0)
    val neuron = new Neuron(inputs, Array.fill(inputDimensionality+1)(0.0), ThresholdFunctionMinToPos)
    (inputs, List(neuron), List(neuron))
  }
  val neuron: Neuron = neurons.head

  //val eta = new Trainable.ConstantEta(.01)
  val eta = Trainable.SearchThenConverge(.005, 3)

  override def trainBody(data: Array[Double], result: Array[Double], n: Int): Unit = {
    val currentOutput = output(data).head
    for((input, index) <- neuron.inputs.zipWithIndex) {
      input.weight = input.weight+eta(n)*(result.head - currentOutput)*(if (index == 0) 1.0 else data(index - 1))
    }
  }
}*/
