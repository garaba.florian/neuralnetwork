package neuralnetwork.ui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.stage.Stage;

import java.io.IOException;

public class FXApp extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(FXApp.class.getResource("/ui.fxml"));

        FXController fxController = new FXController();
        fxmlLoader.setController(fxController);
        SplitPane sp = fxmlLoader.load();

        Scene scene = new Scene(sp, 1900, 1000);
        scene.getStylesheets().add("/ui.css");
        stage.setScene(scene);
        stage.setMaximized(true);
        stage.show();
    }
}
