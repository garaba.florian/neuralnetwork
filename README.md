# Neurális hálózat

Ez egy Scala-ban írt, általános, tanítható neurális hálózat, mely jelen esetben egy véletlenszerűen generált adathalmaz alapján megtanulja szétválasztani a két alakzatot egymástól - a hálózat paraméterei egy JavaFX felületen állíthatóak. A program a hálózat állapotát "élőben" mutatja a tanulás folyamata során.


## Telepítés, futtatás

Oracle SDK 10 és scala 2.12.7 alatt történt a compilation, [innen](neuralnetwork.tar.gz) letölthető egy tar.gz-be csomagolt, futtatható fájl.

Gradle build system:
```
./gradlew build
```

## Program

[Kép](Screenshot.png)
[Videó](https://www.youtube.com/watch?v=sa3p6_wDPXA)
